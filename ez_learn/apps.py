from django.apps import AppConfig


class EzLearnConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ez_learn'
