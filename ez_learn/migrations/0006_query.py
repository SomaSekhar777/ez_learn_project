# Generated by Django 4.2.2 on 2023-08-03 09:00

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('ez_learn', '0005_alter_learnings_course_learning'),
    ]

    operations = [
        migrations.CreateModel(
            name='Query',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('query', models.CharField(max_length=2000)),
                ('learner', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ez_learn.learner')),
            ],
        ),
    ]
