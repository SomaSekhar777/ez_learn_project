# Generated by Django 4.2.2 on 2023-08-01 10:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ez_learn', '0003_learner_city_learner_country_learner_dno_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='learnings',
            name='activation',
            field=models.BooleanField(blank=True, default=False, null=True),
        ),
    ]
