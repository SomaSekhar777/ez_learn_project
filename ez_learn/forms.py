from django import forms
from .models import Learner

class imageForm(forms.Form):
    profile_picture = forms.ImageField()
